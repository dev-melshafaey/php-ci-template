## [4.2.1](https://gitlab.com/to-be-continuous/php/compare/4.2.0...4.2.1) (2023-01-27)


### Bug Fixes

* "Add registry name in all Docker images" ([544121b](https://gitlab.com/to-be-continuous/php/commit/544121b0dd380d78cdb085549a58d61698bea8bc))

# [4.2.0](https://gitlab.com/to-be-continuous/php/compare/4.1.0...4.2.0) (2023-01-07)


### Features

* add composer outdated job (manual) ([447e54c](https://gitlab.com/to-be-continuous/php/commit/447e54c5b108515a8bd6908a1141c50a8d115227))

# [4.1.0](https://gitlab.com/to-be-continuous/php/compare/4.0.0...4.1.0) (2022-11-30)


### Features

* add a job generating software bill of materials ([1624b74](https://gitlab.com/to-be-continuous/php/commit/1624b74a685d1adb2f34942292d4be9c3bb20d7c))

# [4.0.0](https://gitlab.com/to-be-continuous/php/compare/3.0.0...4.0.0) (2022-10-04)


### Features

* normalize reports ([e06bf60](https://gitlab.com/to-be-continuous/php/commit/e06bf609dba33e10fdd8a6bcc4a10d0b77d4d4f7))


### BREAKING CHANGES

* generated reports have changed (see doc). It is a breaking change if you're using SonarQube.

# [3.0.0](https://gitlab.com/to-be-continuous/php/compare/2.1.0...3.0.0) (2022-08-05)


### Features

* adaptive pipeline ([4e26777](https://gitlab.com/to-be-continuous/php/commit/4e26777c35f2986922064312a94ac7a58c4172ba))


### BREAKING CHANGES

* change default workflow from Branch pipeline to MR pipeline

# [2.1.0](https://gitlab.com/to-be-continuous/php/compare/2.0.1...2.1.0) (2022-05-01)


### Features

* configurable tracking image ([143750e](https://gitlab.com/to-be-continuous/php/commit/143750e328ce32eb563e94a8273bec80672ffbc7))

## [2.0.1](https://gitlab.com/to-be-continuous/php/compare/2.0.0...2.0.1) (2021-10-07)


### Bug Fixes

* use master or main for production env ([bbd27da](https://gitlab.com/to-be-continuous/php/commit/bbd27daa95dfb18fd417e365b7742deeb451b1a2))

## [2.0.0](https://gitlab.com/to-be-continuous/php/compare/1.2.1...2.0.0) (2021-09-08)

### Bug Fixes

* add rule according readme ([1728d90](https://gitlab.com/to-be-continuous/php/commit/1728d902b012f10d05fe32222bfa3525ecef8857))
* remove duplicated rule ([046e443](https://gitlab.com/to-be-continuous/php/commit/046e443af6911823ef4a001fb62d14dea35c94f8))

### Features

* Change boolean variable behaviour ([3d9fb3c](https://gitlab.com/to-be-continuous/php/commit/3d9fb3c294122aba56c11d1c190ed3d388a49f96))

### BREAKING CHANGES

* boolean variable now triggered on explicit 'true' value

## [1.2.1](https://gitlab.com/to-be-continuous/php/compare/1.2.0...1.2.1) (2021-07-05)

### Bug Fixes

* **phpunit:** enable xdebug coverage ([6914a5a](https://gitlab.com/to-be-continuous/php/commit/6914a5aae4b66ef5f22a1f18364aa57b4e40d6ee))

## [1.2.0](https://gitlab.com/to-be-continuous/php/compare/1.1.1...1.2.0) (2021-06-10)

### Features

* move group ([79bc16f](https://gitlab.com/to-be-continuous/php/commit/79bc16f70fcc5628bb1529e16f75850757b3dd02))

## [1.1.1](https://gitlab.com/Orange-OpenSource/tbc/php/compare/1.1.0...1.1.1) (2021-06-09)

### Bug Fixes

* **phpunit:** remove variable from rules:exists expression (unsupported) ([e805d5f](https://gitlab.com/Orange-OpenSource/tbc/php/commit/e805d5ff4790679e9d06533778889c3792f31008))

## [1.1.0](https://gitlab.com/Orange-OpenSource/tbc/php/compare/1.0.0...1.1.0) (2021-05-18)

### Features

* add scoped variables support ([0722911](https://gitlab.com/Orange-OpenSource/tbc/php/commit/07229110744a1bb43de2337ec86ec300f96f55cf))

## 1.0.0 (2021-05-06)

### Features

* initial release ([aef221a](https://gitlab.com/Orange-OpenSource/tbc/php/commit/aef221aa4f151e477c62563e1fb229d755a7906f))
