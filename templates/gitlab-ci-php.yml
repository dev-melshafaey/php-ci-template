# =========================================================================================
# Copyright (C) 2021 Orange & contributors
#
# This program is free software; you can redistribute it and/or modify it under the terms 
# of the GNU Lesser General Public License as published by the Free Software Foundation; 
# either version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License along with this 
# program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth 
# Floor, Boston, MA  02110-1301, USA.
# =========================================================================================
# default workflow rules: Merge Request pipelines
workflow:
  rules:
    # prevent branch pipeline when an MR is open (prefer MR pipeline)
    - if: '$CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS'
      when: never
    - when: always

# test job prototype: implement adaptive pipeline rules
.test-policy:
  rules:
    # on tag: auto & failing
    - if: $CI_COMMIT_TAG
    # on ADAPTIVE_PIPELINE_DISABLED: auto & failing
    - if: '$ADAPTIVE_PIPELINE_DISABLED == "true"'
    # on production or integration branch(es): auto & failing
    - if: '$CI_COMMIT_REF_NAME =~ $PROD_REF || $CI_COMMIT_REF_NAME =~ $INTEG_REF'
    # early stage (dev branch, no MR): manual & non-failing
    - if: '$CI_MERGE_REQUEST_ID == null && $CI_OPEN_MERGE_REQUESTS == null'
      when: manual
      allow_failure: true
    # Draft MR: auto & non-failing
    - if: '$CI_MERGE_REQUEST_TITLE =~ /^Draft:.*/'
      allow_failure: true
    # else (Ready MR): auto & failing
    - when: on_success

variables:
  # variabilized tracking image
  TBC_TRACKING_IMAGE: "$CI_REGISTRY/to-be-continuous/tools/tracking:master"

  # PHP image (can be overridden)
  PHP_IMAGE: "registry.hub.docker.com/library/php:latest"

  PHP_PROJECT_DIR: "."

  # Version of the cyclonedx/cyclonedx-php-composer used for SBOM analysis
  PHP_SBOM_OPTS: "--exclude-plugins --exclude-dev"
  PHP_OUTDATED_OPTS: "--direct"

  # default production ref name (pattern)
  PROD_REF: '/^(master|main)$/'
  # default integration ref name (pattern)
  INTEG_REF: '/^develop$/'

stages:
  - build
  - test
  - publish

.php-scripts: &php-scripts |
  # BEGSCRIPT
  set -e

  function log_info() {
      echo -e "[\\e[1;94mINFO\\e[0m] $*"
  }

  function log_warn() {
      echo -e "[\\e[1;93mWARN\\e[0m] $*"
  }

  function log_error() {
      echo -e "[\\e[1;91mERROR\\e[0m] $*"
  }

  function install_ca_certs() {
    certs=$1
    if [[ -z "$certs" ]]
    then
      return
    fi

    # import in system
    if echo "$certs" >> /etc/ssl/certs/ca-certificates.crt
    then
      log_info "CA certificates imported in \\e[33;1m/etc/ssl/certs/ca-certificates.crt\\e[0m"
    fi
    if echo "$certs" >> /etc/ssl/cert.pem
    then
      log_info "CA certificates imported in \\e[33;1m/etc/ssl/cert.pem\\e[0m"
    fi
  }

  function unscope_variables() {
    _scoped_vars=$(env | awk -F '=' "/^scoped__[a-zA-Z0-9_]+=/ {print \$1}" | sort)
    if [[ -z "$_scoped_vars" ]]; then return; fi
    log_info "Processing scoped variables..."
    for _scoped_var in $_scoped_vars
    do
      _fields=${_scoped_var//__/:}
      _condition=$(echo "$_fields" | cut -d: -f3)
      case "$_condition" in
      if) _not="";;
      ifnot) _not=1;;
      *)
        log_warn "... unrecognized condition \\e[1;91m$_condition\\e[0m in \\e[33;1m${_scoped_var}\\e[0m"
        continue
      ;;
      esac
      _target_var=$(echo "$_fields" | cut -d: -f2)
      _cond_var=$(echo "$_fields" | cut -d: -f4)
      _cond_val=$(eval echo "\$${_cond_var}")
      _test_op=$(echo "$_fields" | cut -d: -f5)
      case "$_test_op" in
      defined)
        if [[ -z "$_not" ]] && [[ -z "$_cond_val" ]]; then continue; 
        elif [[ "$_not" ]] && [[ "$_cond_val" ]]; then continue; 
        fi
        ;;
      equals|startswith|endswith|contains|in|equals_ic|startswith_ic|endswith_ic|contains_ic|in_ic)
        # comparison operator
        # sluggify actual value
        _cond_val=$(echo "$_cond_val" | tr '[:punct:]' '_')
        # retrieve comparison value
        _cmp_val_prefix="scoped__${_target_var}__${_condition}__${_cond_var}__${_test_op}__"
        _cmp_val=${_scoped_var#"$_cmp_val_prefix"}
        # manage 'ignore case'
        if [[ "$_test_op" == *_ic ]]
        then
          # lowercase everything
          _cond_val=$(echo "$_cond_val" | tr '[:upper:]' '[:lower:]')
          _cmp_val=$(echo "$_cmp_val" | tr '[:upper:]' '[:lower:]')
        fi
        case "$_test_op" in
        equals*)
          if [[ -z "$_not" ]] && [[ "$_cond_val" != "$_cmp_val" ]]; then continue; 
          elif [[ "$_not" ]] && [[ "$_cond_val" == "$_cmp_val" ]]; then continue; 
          fi
          ;;
        startswith*)
          if [[ -z "$_not" ]] && [[ "$_cond_val" != "$_cmp_val"* ]]; then continue; 
          elif [[ "$_not" ]] && [[ "$_cond_val" == "$_cmp_val"* ]]; then continue; 
          fi
          ;;
        endswith*)
          if [[ -z "$_not" ]] && [[ "$_cond_val" != *"$_cmp_val" ]]; then continue; 
          elif [[ "$_not" ]] && [[ "$_cond_val" == *"$_cmp_val" ]]; then continue; 
          fi
          ;;
        contains*)
          if [[ -z "$_not" ]] && [[ "$_cond_val" != *"$_cmp_val"* ]]; then continue; 
          elif [[ "$_not" ]] && [[ "$_cond_val" == *"$_cmp_val"* ]]; then continue; 
          fi
          ;;
        in*)
          if [[ -z "$_not" ]] && [[ "__${_cmp_val}__" != *"__${_cond_val}__"* ]]; then continue; 
          elif [[ "$_not" ]] && [[ "__${_cmp_val}__" == *"__${_cond_val}__"* ]]; then continue; 
          fi
          ;;
        esac
        ;;
      *)
        log_warn "... unrecognized test operator \\e[1;91m${_test_op}\\e[0m in \\e[33;1m${_scoped_var}\\e[0m"
        continue
        ;;
      esac
      # matches
      _val=$(eval echo "\$${_target_var}")
      log_info "... apply \\e[32m${_target_var}\\e[0m from \\e[32m\$${_scoped_var}\\e[0m${_val:+ (\\e[33;1moverwrite\\e[0m)}"
      _val=$(eval echo "\$${_scoped_var}")
      export "${_target_var}"="${_val}"
    done
    log_info "... done"
  }

  # install PHP system dependencies
  function install_project_deps() {
    if [[ -f ".php_apt_get" ]]
    then
      log_info "file \\e[33;1m.php_apt_get\\e[0m found: install project system dependencies..."
      # shellcheck disable=SC2046
      apt-get install -yqq $(cat .php_apt_get)
    fi

    if [[ -f ".php_pecl" ]]
    then
      log_info "file \\e[33;1m.php_pecl\\e[0m file found: install project extensions..."
      # set proxy for pear
      # shellcheck disable=SC2154
      if [[ "${http_proxy}"  ]]; 
      then
        pear config-set http_proxy "${http_proxy}"
      fi
      # shellcheck disable=SC2046
      pecl install $(cat .php_pecl)
    fi

    if [[ -f ".php_ext" ]]
    then
      log_info "file \\e[33;1m.php_ext\\e[0m file found: install project extensions..."
      # shellcheck disable=SC2046
      docker-php-ext-install $(cat .php_ext)
    fi
  }

  function docomposer() {
    if command -v composer > /dev/null
    then
      # composer is available in this image: use it
      composer "$@"
    else
      if [[ ! -f "composer.phar" ]]
      then
        # download & install composer
        log_info "composer not found in the image: install..."
        curl --show-error --silent https://getcomposer.org/installer | php
      fi
      php composer.phar "$@"
    fi
  }

  # appends xdebug.mode=coverage in php.ini file
  function set_xdebug_mode() {
    phpini_path=$(php -i | grep php.ini | sed -e 's/.*=> *//')
    echo 'xdebug.mode=coverage' >> "${phpini_path}/conf.d/xdebug_coverage.ini"
    log_info "xdebug.mode=coverage set in \\e[33;1m${phpini_path}/conf.d/xdebug_coverage.ini\\e[0m"
  }

  # locates and executes phpunit (./vendor/bin/phpunit is default location; ./bin/phpunit alternate location)
  function dophpunit() {
    if [[ -f "./vendor/bin/phpunit" ]]
    then
      ./vendor/bin/phpunit "$@"
    elif [[ -f "./bin/phpunit" ]]
    then
      ./bin/phpunit "$@"
    else
      log_error "phpunit not found: add it as a project requirement (\\e[33;1mcomposer require --dev phpunit/phpunit\\e[0m)"
      exit 1
    fi
  }

  unscope_variables

  # ENDSCRIPT

# Generic php job
.php-base:
  image: $PHP_IMAGE
  services:
    - name: "$TBC_TRACKING_IMAGE"
      command: ["--service", "php", "4.2.1" ]
  before_script:
    - *php-scripts
    - install_ca_certs "${CUSTOM_CA_CERTS:-$DEFAULT_CA_CERTS}"
    - cd $PHP_PROJECT_DIR
    # install zip & unzip (required to unzip dist packages) git (required by composer)
    - apt-get update -yqq
    - apt-get install zip unzip git -yqq
    # install project system dependencies (if any)
    - install_project_deps
    # install project dependencies
    - docomposer install --no-interaction --no-progress --prefer-dist
    - mkdir -p -m 777 reports
  # Cache downloaded dependencies and plugins between builds.
  # To keep cache across branches add 'key: "$CI_JOB_NAME"'
  cache:
    key: "$CI_COMMIT_REF_SLUG-php"
    paths:
      - $PHP_PROJECT_DIR/vendor/

php-unit:
  extends: .php-base
  stage: build
  script:
    # install Xdebug
    - |
      # set proxy for pear
      if [[ "${http_proxy}" ]];
      then
        pear config-set http_proxy "${http_proxy}"
      fi
    - pecl install xdebug
    - docker-php-ext-enable xdebug
    - set_xdebug_mode
    # run PHPUnit (no color is important for the coverage regex)
    - dophpunit ${TRACE+--debug} --coverage-text --log-junit=reports/php-test.xunit.xml --coverage-cobertura=reports/php-coverage.cobertura.xml --coverage-clover=reports/php-coverage.clover.xml --colors=never $PHP_UNIT_ARGS
  # code coverage RegEx
  coverage: '/^\s*Lines:\s*(\d+\.?\d*\%)/'
  # keep build artifacts and JUnit reports
  artifacts:
    name: "$CI_JOB_NAME artifacts from $CI_PROJECT_NAME on $CI_COMMIT_REF_SLUG"
    expire_in: 1 day
    when: always
    reports:
      junit:
        - "$PHP_PROJECT_DIR/reports/php-test.xunit.xml"
      coverage_report:
        coverage_format: cobertura
        path: "$PHP_PROJECT_DIR/reports/php-coverage.cobertura.xml"
    paths:
      - "$PHP_PROJECT_DIR/reports/php-test.*"
      - "$PHP_PROJECT_DIR/reports/php-coverage.*"
  rules:
    - if: '$PHP_UNIT_DISABLED == "true"'
      when: never
    - !reference [.test-policy, rules]

php-outdated:
  extends: .php-base
  stage: test
  dependencies: []
  script:
    - docomposer outdated --strict $PHP_OUTDATED_OPTS
  rules:
    # on schedule: auto
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
      allow_failure: true
    # else manual & non-blocking
    - when: manual
      allow_failure: true

php-codesniffer:
  stage: test
  extends: .php-base
  script:
    # install PHP_CodeSniffer
    - docomposer global require "squizlabs/php_codesniffer=*" --no-interaction --no-progress --prefer-stable
    - $(docomposer global config bin-dir --absolute)/phpcs --report-summary --report-checkstyle=reports/php-codesniffer.checkstyle.xml $PHP_CODESNIFFER_ARGS
  artifacts:
    name: "$CI_JOB_NAME artifacts from $CI_PROJECT_NAME on $CI_COMMIT_REF_SLUG"
    expire_in: 1 day
    when: always
    paths:
      - "$PHP_PROJECT_DIR/reports/php-codesniffer.*"
  rules:
    # exclude if $PHP_CODESNIFFER_DISABLED not set
    - if: '$PHP_CODESNIFFER_DISABLED == "true"'
      when: never
    - !reference [.test-policy, rules]

php-sbom:
  extends: .php-base
  stage: test
  # force no dependency
  dependencies: []
  script:
    - mkdir -p -m 777 reports
    - docomposer global config --no-plugins allow-plugins.cyclonedx/cyclonedx-php-composer true
    - docomposer global require "cyclonedx/cyclonedx-php-composer${PHP_SBOM_VERSION:+:$PHP_SBOM_VERSION}"
    - docomposer make-bom --output-file=reports/php-sbom.cyclonedx.json --output-format=JSON --no-interaction $PHP_SBOM_OPTS
    - chmod a+r reports/php-sbom.cyclonedx.json
  artifacts:
    name: "SBOM for PHP from $CI_PROJECT_NAME on $CI_COMMIT_REF_SLUG"
    expire_in: 1 week
    when: always
    paths:
      - $PHP_PROJECT_DIR/reports/php-sbom.cyclonedx.json
  rules:
    # exclude if disabled
    - if: '$PHP_SBOM_DISABLED == "true"'
      when: never
    - !reference [.test-policy, rules]
